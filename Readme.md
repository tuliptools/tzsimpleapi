# TzSimpleAPI

This is a simple API that relies on a Tezos Archive Node + conseil

## buidl

you can build this locally if you have go installed with

```
go build main.go
```

you can also build a docker image with

```
docker build -t tezsimple .
```

or you can the image from dockerhub:

```
// TODO
docker pull tuliptools/tezsimple:latest
```

## config

the binary expects a file called `config.yaml` in one of these directories:

* ./
* /home/.tezsimple/
* /etc/tezsimple/

with content like:

```
ConseilHost: "https://conseil-prod.cryptonomic-infra.tech"
ConseilToken: "galleon"
ConseilPlatform: "tezos"
ConseilNetwork: "mainnet"
TezosHost: "https://rpc.tezrpc.me/"
ApiPort: 8885
```


as well as a file  called `knownAddresses.json` for address <-> name mappings,
you can copy paste the one from this git repo


## api

The following routes are implemented:

```
/v3/account_from_alias/:alias
/v3/account_status/:account_hash
/v3/accounts
/v3/baker_rights/:account_hash
/v3/bakings/:account_hash
/v3/bakings_endorsement/:account_hash
/v3/balance/:account_hash
/v3/balance_from_balance_updates/:account_hash
/v3/balance_history/:account_hash
/v3/balance_updates/:account_hash
/v3/head
/v3/genesis
/v3/constants
/v3/heads
/v3/block/:block_hash
/v3/network/:block_hash
/v3/operations/:block_hash
/v3/level/:block_hash
/v3/roll_number/:account_hash
/v3/endorser_rights/:account_hash
/v3/block_level/:level
/v3/block_next/:block_hash
/v3/block_prev/:block_hash
/v3/block_hash_level/:level
/v3/volume/:block_hash
/v3/timestamp/:block_hash
/v3/priority/:block_hash
/v3/number_operations/:hash
/v3/number_accounts
/v3/votes_account/:account_hash
/v3/ballots/:period
/v3/cycle_baker_rights/:account_hash
/v3/number_baker_rights/:account_hash
/v3/number_baking_rights
/v3/number_bakings/:account_hash
/v3/number_bakings_endorsement/:account_hash
/v3/number_operations
```