package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	file, err := os.Open("./bakers.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		a := scanner.Text()
		a = strings.Split(a, ",")[1]
		fmt.Println("get for " + a)
		get(a)

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func get(addr string) {
	url := "http://api6.tzscan.io/v3/rewards_split/" + addr
	res, err := http.Get(url)
	if err != nil {

	}
	body, _ := ioutil.ReadAll(res.Body)

	f, err := os.Create("./rewards_split/" + addr)
	fmt.Println(err)
	f.Write(body)
	f.Close()
}
