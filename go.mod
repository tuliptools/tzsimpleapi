module gitlab.com/tuliptools/tzsimplepi

go 1.13

require (
	github.com/DefinitelyNotAGoat/go-tezos v1.0.9
	github.com/cstockton/go-conv v0.0.0-20170524002450-66a2b2ba36e1
	github.com/gin-gonic/gin v1.4.0
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/pelletier/go-toml v1.5.0 // indirect
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.4.0
	github.com/uber-go/dig v1.7.0 // indirect
	gitlab.com/tuliptools/ConseilGO v0.0.0-20191015084333-730ec00107d3 // indirect
	go.uber.org/dig v1.7.0 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	golang.org/x/text v0.3.2 // indirect
)
