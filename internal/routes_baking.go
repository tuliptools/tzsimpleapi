package internal

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

func (this *Api) numBakingRights(c *gin.Context) {
	cycle := this.validateInt(c, "cycle", 1)
	addr := c.Param("account_hash")

	rights := this.getbakingRights(addr, cycle, 6)

	c.JSON(200, []int{len(rights)})
}

func (this *Api) numBakings(c *gin.Context) {
	cycle := this.validateInt(c, "cycle", 1)
	addr := c.Param("account_hash")

	rights := this.getbakingRights(addr, cycle, 1)

	c.JSON(200, []int{len(rights)})
}

func (this *Api) endorsements_history(c *gin.Context) {

}

func (this *Api) numEndorsements(c *gin.Context) {
	cycle := this.validateInt(c, "cycle", 1)
	addr := c.Param("account_hash")

	rights := this.getendorsingrights(addr, cycle)

	c.JSON(200, []int{len(rights)})
}

func (this *Api) bakerRights(c *gin.Context) {
	cycle := this.validateInt(c, "cycle", 1)
	// p := this.validateInt(c,"p",0)
	// number := this.validateInt(c,"number",100)
	addr := c.Param("account_hash")

	rights := this.getbakingRights(addr, cycle, 5)

	brres := []BakerRights{}

	for _, i := range rights {
		brres = append(brres, BakerRights{
			Level:    i.Level,
			Cycle:    i.Level / 4096,
			Priority: i.Priority,
			Depth:    0,
		})
	}

	c.JSON(200, brres)
}

func (this *Api) getbakingRights(addr string, cycle, max int) []RpcBakingRights {
	res, _ := http.Get(this.config.TezosHost + "/chains/main/blocks/head/helpers/baking_rights?delegate=" + addr + "&cycle=" + strconv.Itoa(cycle) + "&max_priority=" + strconv.Itoa(max))
	all, _ := ioutil.ReadAll(res.Body)
	rights := []RpcBakingRights{}
	json.Unmarshal(all, &rights)
	return rights
}

func (this *Api) endorserRights(c *gin.Context) {
	addr := c.Param("account_hash")
	// p := this.validateInt(c,"p",0)
	// number := this.validateInt(c,"number",1000)
	cycle := this.validateInt(c, "cycle", 160)

	ret := []EndorserRights{}
	res, _ := http.Get(this.config.TezosHost + "/chains/main/blocks/head/helpers/endorsing_rights?delegate=" + addr + "&cycle=" + strconv.Itoa(cycle))
	all, _ := ioutil.ReadAll(res.Body)
	rights := []RPCEndrosementRights{}
	json.Unmarshal(all, &rights)

	for _, i := range rights {
		ret = append(ret, EndorserRights{
			Level: i.Level,
			Cycle: cycle,
			Nslot: len(i.Slots),
			Depth: i.Level - (cycle * 4096),
		})
	}

	c.JSON(200, ret)
}

func (this *Api) bakings(c *gin.Context) {
	addr := c.Param("account_hash")
	cycle := this.validateInt(c, "cycle", 1)

	// baking rights
	res, _ := http.Get(this.config.TezosHost + "/chains/main/blocks/head/helpers/baking_rights?delegate=" + addr + "&cycle=" + strconv.Itoa(cycle) + "&max_priority=1")
	all, _ := ioutil.ReadAll(res.Body)
	rights := []RpcBakingRights{}

	json.Unmarshal(all, &rights)

	// ask conseil for actual baked blocks
	levels := []string{}
	for _, right := range rights {
		levels = append(levels, strconv.Itoa(right.Level))
	}
	q := this.conseil.NewQuery()
	q.AddFields("hash", "level", "priority", "meta_cycle", "timestamp", "baker")
	q.AddPredicate("level", "in", levels, false)
	q.SetLimit(1000)
	conseilRes, _ := this.conseil.Exec(q, "blocks")

	// TODO FEES
	this.conseil.WaitFor(conseilRes)
	conbaked := []ConBlocks{}
	json.Unmarshal(conseilRes.Bytes(), &conbaked)

	result := []Bakings{}
	for _, right := range rights {
		bake := Bakings{}
		bake.Level = right.Level
		for _, actual := range conbaked {
			bake.BlockHash = actual.Hash
			if actual.Level == right.Level {
				bake.Timestamp = time.Unix(actual.Timestamp/1000, 0)
				bake.BakerHash = this.getAccount(actual.Baker)
				bake.Priority = actual.Priority
				if bake.Priority >= 1 {
					bake.BakeTime = 60 + 15*bake.Priority
				}
				if bake.BakerHash.Tz != addr {
					bake.MissedPriority = right.Priority
					bake.Baked = false
				} else {
					bake.Baked = true
				}
				result = append(result, bake)
			}
		}
	}

	c.JSON(200, result)
}

func (this *Api) baking_endorsements(c *gin.Context) {
	addr := c.Param("account_hash")
	cycle := this.validateInt(c, "cycle", 1)

	// endorsing rights
	rights := this.getendorsingrights(addr, cycle)

	// ask conseil for actual endorsements
	levels := []string{}
	for _, right := range rights {
		levels = append(levels, strconv.Itoa(right.Level))
	}
	q := this.conseil.NewQuery()
	q.AddFields("kind", "number_of_slots", "level", "slots", "block_hash", "timestamp")
	q.AddPredicate("kind", "in", []string{"endorsement"}, false)
	q.AddPredicate("delegate", "eq", []string{addr}, false)
	q.AddPredicate("level", "in", levels, false)
	q.SetLimit(1000)
	conseilRes, err := this.conseil.Exec(q, "operationsBH")

	fmt.Println(err)

	this.conseil.WaitFor(conseilRes)

	conend := []ConEndorsement{}

	json.Unmarshal(conseilRes.Bytes(), &conend)

	accountStruct := this.getAccount(addr)
	result := []BakingsEndorsement{}
	for _, right := range rights {
		endorsement := BakingsEndorsement{}
		endorsement.Level = right.Level
		for _, actual := range conend {
			endorsement.Block = actual.BlockHash
			if actual.Level == right.Level {
				endorsement.Block = actual.BlockHash
				endorsement.Timestamp = time.Unix(actual.Timestamp/1000, 0)
				endorsement.Priority = 0
				slots := []int{}
				json.Unmarshal([]byte(actual.Slots), &slots)
				endorsement.Slots = slots
				endorsement.Source = accountStruct
				endorsement.Cycle = cycle
				endorsement.LrNslot = actual.NumberOfSlots
				result = append(result, endorsement)
			}
		}
	}

	c.JSON(200, result)
}

func (this *Api) getendorsingrights(addr string, cycle int) []RPCEndrosementRights {
	res, _ := http.Get(this.config.TezosHost + "/chains/main/blocks/head/helpers/endorsing_rights?delegate=" + addr + "&cycle=" + strconv.Itoa(cycle))
	all, _ := ioutil.ReadAll(res.Body)
	rights := []RPCEndrosementRights{}
	json.Unmarshal(all, &rights)
	return rights
}
