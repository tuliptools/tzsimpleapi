package internal

import (
	"fmt"
	gotezos "github.com/DefinitelyNotAGoat/go-tezos"
	"github.com/gin-gonic/gin"
	"gitlab.com/tuliptools/tzsimplepi/conseil"
	"strconv"
	"strings"
	"github.com/gin-contrib/cors"
	"time"
)

type Api struct {
	port          int
	knownAccounts KnownAccounts
	conseil       *conseil.Conseil
	goTezos       *gotezos.GoTezos
	config        *TzSimpleConfig
}

func (this *Api) Run() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowHeaders:     []string{"*"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return true
		},
		MaxAge: 12 * time.Hour,
	}))
	r.Use(gin.Recovery())
	fmt.Println("run2")
	this.routes(r)
	fmt.Println("starting Server on :" + strconv.Itoa(this.port))
	fmt.Println(r.Run(":" + strconv.Itoa(this.port)))
}

func NewAPI(config *TzSimpleConfig, conseil *conseil.Conseil, gotez *gotezos.GoTezos) *Api {
	a := Api{}
	a.port = config.ApiPort
	a.knownAccounts = config.KnownAccounts
	a.conseil = conseil
	a.goTezos = gotez
	a.config = config
	return &a
}

func (this *Api) dummyHandler(c *gin.Context) {
	c.String(200, "TODO")
}

// helpers

func (this *Api) cleanString(s string) string {
	s = strings.Trim(s, " ")
	s = strings.ToLower(s)
	return s
}
