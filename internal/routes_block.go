package internal

import (
	"encoding/json"
	"fmt"
	gotezos "github.com/DefinitelyNotAGoat/go-tezos"
	"github.com/cstockton/go-conv"
	"github.com/gin-gonic/gin"
	"strconv"
)

func (this *Api) head(c *gin.Context) {
	head, _ := this.goTezos.Block.GetHead()
	ops := this.validateBool(c, "operationsBH", false)

	res := this.toBlock(head, ops)
	c.JSON(200, res)
}

func (this *Api) block(c *gin.Context) {
	head, _ := this.goTezos.Block.Get(c.Param("block_hash"))
	ops := this.validateBool(c, "operationsBH", false)

	res := this.toBlock(head, ops)
	c.JSON(200, res)
}

func (this *Api) blocks(C *gin.Context) {

}

func (this *Api) operationEntry(c *gin.Context) {
	param := c.Param("param")
	if len(param) >= 5 {
		if param[:2] == "tz" || param[:2] == "KT" {
			this.opAddr2(c)
		} else {
			this.operationsBlock(c)
		}
	}
}

func (this *Api) operationsBlock(c *gin.Context) {
	block, _ := this.goTezos.Block.Get(c.Param("block_hash"))
	optype := c.Param("type")
	res := []SimpleOperation{}

	for _, o := range block.Operations {
		for _, oo := range o {
			for _, c := range oo.Contents {
				n := SimpleOperation{
					Hash:      oo.Hash,
					BlockHash: block.Hash,
					Type: OpType{
						Kind:      c.Kind,
						Block:     oo.Branch,
						Level:     block.Header.Level,
						OpLevel:   block.Header.Level + 1,
						Priority:  -1,
						Timestamp: "",
					},
				}
				if optype == "" {
					res = append(res, n)
				} else {
					if optype == n.Type.Kind {
						res = append(res, n)
					}
				}
			}
		}
	}
	c.JSON(200, res)
}

func (this *Api) level(c *gin.Context) {
	res := Level{}
	bh := c.Param("block_hash")
	block, _ := this.goTezos.Block.Get(bh)

	res.Level = block.Header.Level
	res.Cycle = (res.Level / 4096)
	res.LevelPosition = res.Level - 1
	res.CyclePosition = -1 + res.Level - res.Cycle*4096
	// TODO
	// res.VotingPeriod
	c.JSON(200, res)
}

func (this *Api) blockChainId(c *gin.Context) {
	hash := c.Param("block_hash")
	head, _ := this.goTezos.Block.Get(hash)
	c.JSON(200, []string{head.ChainID})
}

func (this *Api) blockLevel(c *gin.Context) {
	level := c.Param("level")
	operations := this.validateBool(c, "operationsBH", false)

	// ask conseil for blockhash
	// then get via rpc
	qry := this.conseil.NewQuery()
	qry.AddFields("hash")
	qry.AddPredicate("level", "eq", []string{level}, false)
	qry.SetLimit(1)

	res, _ := this.conseil.Exec(qry, "blocks")
	res.Wait()

	conres := []ConBlocks{}
	json.Unmarshal(res.Bytes(), &conres)
	if len(conres) == 0 {
		c.String(400, "level not found")
		return
	}

	block, _ := this.goTezos.Block.Get(conres[0].Hash)

	fmt.Println(operations)
	c.JSON(200, this.toBlock(block, operations))

}

func (this *Api) blockNext(c *gin.Context) {

	// returns []string{bh}
	qry := this.conseil.NewQuery()
	qry.AddFields("hash")
	qry.AddPredicate("predecessor", "eq", []string{c.Param("block_hash")}, false)
	qry.SetLimit(1)
	res, _ := this.conseil.Exec(qry, "blocks")
	res.Wait()

	conres := []ConBlocks{}
	json.Unmarshal(res.Bytes(), &conres)
	if len(conres) == 0 {
		c.String(400, "hash not found")
		return
	}

	ret := []string{}
	for _, i := range conres {
		ret = append(ret, i.Hash)
	}

	c.JSON(200, ret)

}

func (this *Api) blocksPrev(c *gin.Context) {
	// returns []string{bh}
	qry := this.conseil.NewQuery()
	qry.AddFields("predecessor")
	qry.AddPredicate("hash", "eq", []string{c.Param("block_hash")}, false)
	qry.SetLimit(1)
	res, _ := this.conseil.Exec(qry, "blocks")
	res.Wait()

	conres := []ConBlocks{}
	json.Unmarshal(res.Bytes(), &conres)
	if len(conres) == 0 {
		c.String(400, "hash not found")
		return
	}

	c.JSON(200, []string{conres[0].Predecessor})

}

func (this *Api) priority(c *gin.Context) {
	qry := this.conseil.NewQuery()
	qry.AddFields("priority")
	qry.AddPredicate("hash", "eq", []string{c.Param("block_hash")}, false)
	qry.SetLimit(1)
	res, _ := this.conseil.Exec(qry, "blocks")
	res.Wait()

	conres := []ConBlocks{}
	json.Unmarshal(res.Bytes(), &conres)
	if len(conres) == 0 {
		c.String(400, "hash not found")
		return
	}

	c.JSON(200, []string{strconv.Itoa(conres[0].Priority)})
}

func (this *Api) numOps(c *gin.Context) {
	hash := c.Param("hash")

	block, _ := this.goTezos.Block.Get(hash)
	num := 0

	for _, o := range block.Operations {
		num += len(o)
	}

	c.JSON(200, []int{num})
}

func (this *Api) blockHashLevel(c *gin.Context) {
	// returns []string{bh}
	qry := this.conseil.NewQuery()
	qry.AddFields("hash")
	qry.AddPredicate("level", "eq", []string{c.Param("level")}, false)
	qry.SetLimit(1)
	res, _ := this.conseil.Exec(qry, "blocks")
	res.Wait()

	conres := []ConBlocks{}
	json.Unmarshal(res.Bytes(), &conres)
	if len(conres) == 0 {
		c.String(400, "hash not found")
		return
	}

	c.JSON(200, []string{conres[0].Hash})
}

func (this *Api) volume(c *gin.Context) {
	hash := c.Param("block_hash")

	nbr := func(s string) uint64 {
		u, _ := conv.Uint64(s)
		return u
	}

	block, _ := this.goTezos.Block.Get(hash)
	volume := uint64(0)

	for _, o := range block.Operations {
		for _, oo := range o {
			for _, s := range oo.Contents {
				volume += nbr(s.Amount)
			}
		}
	}

	st, _ := conv.String(volume)
	c.JSON(200, []string{st})

}

func (this *Api) timestamp(c *gin.Context) {
	h := c.Param("block_hash")
	block, _ := this.goTezos.Block.Get(h)

	c.JSON(200, []string{block.Header.Timestamp.String()})
}

func (this *Api) toBlock(head gotezos.Block, ops bool) Head {
	res := Head{}
	res.Hash = head.Hash
	res.PredecessorHash = head.Header.Predecessor
	res.Fitness = head.Header.Fitness[0] + " " + head.Header.Fitness[1]
	res.Timestamp = head.Header.Timestamp
	res.ValidationPass = head.Header.ValidationPass
	res.Protocol.Hash = head.Protocol
	res.Protocol.Name = head.Protocol
	res.Network = "NetXdQprcVkpaWU"
	res.TestNetwork = "Not running"
	res.TestNetworkExpiration = "Not running"
	res.Baker = this.getAccount(head.Metadata.Baker)
	res.NbOperations = len(head.Operations)
	res.Priority = head.Header.Priority
	res.Level = head.Header.Level
	res.CommitedNonceHash, _ = conv.String(head.Metadata.NonceHash)
	res.Proto = head.Header.Proto
	res.Data = "--"
	res.Signature = head.Header.Signature
	if ops {
		res.Operations = head.Operations
	}
	nbr := func(s string) uint64 {
		u, _ := conv.Uint64(s)
		return u
	}
	for _, o := range head.Operations {
		for _, oo := range o {
			for _, ooo := range oo.Contents {
				res.Volume += nbr(ooo.Amount)
				res.Fees += nbr(ooo.Fee)
			}
		}
	}
	return res
}

func (this *Api) genesis(c *gin.Context) {
	head, _ := this.goTezos.Block.Get("BLockGenesisGenesisGenesisGenesisGenesisf79b5d1CoW2")

	res := Head{}
	res.Hash = head.Hash
	res.PredecessorHash = head.Header.Predecessor
	res.Timestamp = head.Header.Timestamp
	res.ValidationPass = head.Header.ValidationPass
	res.Protocol.Hash = head.Protocol
	res.Protocol.Name = head.Protocol
	res.Network = "NetXdQprcVkpaWU"
	res.TestNetwork = "Not running"
	res.TestNetworkExpiration = "Not running"
	res.Baker = this.getAccount(head.Metadata.Baker)
	res.NbOperations = len(head.Operations)
	res.Priority = head.Header.Priority
	res.Level = head.Header.Level
	res.CommitedNonceHash, _ = conv.String(head.Metadata.NonceHash)
	res.Proto = head.Header.Proto
	res.Data = "--"
	res.Signature = head.Header.Signature
	c.JSON(200, res)
}

func (this *Api) constants(c *gin.Context) {
	c.JSON(200, this.goTezos.Constants)
}

func (this *Api) heads(c *gin.Context) {
	c.JSON(200, this.goTezos.Constants)
}
