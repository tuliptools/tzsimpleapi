package internal

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"os"
)

type KnownAccount struct {
	Address string `json:"address"`
	Name    string `json:"name"`
	Logo    string `json:"logo"`
	Site    string `json:"site"`
}

type KnownAccounts []KnownAccount

type TzSimpleConfig struct {
	ApiPort         int
	ConseilHost     string
	ConseilToken    string
	ConseilPlatform string
	ConseilNetwork  string
	TezosHost       string
	KnownAccounts   KnownAccounts // in extra file knownAddresses.json
}

func NewTzSimpleConfig() *TzSimpleConfig {
	s := TzSimpleConfig{}

	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/tzsimple/")
	viper.AddConfigPath("$HOME/.tzsimple")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	viper.Unmarshal(&s)

	jsonFile, err := os.Open("knownAddresses.json")
	if err != nil {
		panic(fmt.Errorf(err.Error()))
	}
	defer jsonFile.Close()

	bytes, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(bytes, &s.KnownAccounts)

	return &s
}
