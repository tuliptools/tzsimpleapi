package internal

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"strconv"
	"strings"
)

func (this *Api) operationsAddr(c *gin.Context) {
	kind := c.Query("type")         // required
	delegate := c.Query("delegate") // required

	qry := this.conseil.NewQuery()
	qry.AddFields("operation_group_hash", "block_hash")
	qry.AddPredicate("kind", "eq", []string{kind}, false)
	qry.AddPredicate("source", "eq", []string{delegate}, false)
	qry.SetLimit(100)

	qres, err := this.conseil.Exec(qry, "operations")
	if err != nil || qry == nil {
		c.String(500, "internal error")
	}
	qres.Wait()
	model := []ConOp{}
	json.Unmarshal(qres.Bytes(), &model)

	blocks := map[string]bool{}

	for _, i := range model {
		blocks[i.BlockHash] = true
	}

	res := []Operation{}

	for i, _ := range blocks {
		b, _ := this.goTezos.Block.Get(i)
		for _, o := range b.Operations {
			for _, oo := range o {
				for _, c := range oo.Contents {
					counter, _ := strconv.Atoi(c.Counter)
					if c.Kind == strings.ToLower(kind) && c.Source == delegate {
						res = append(res, Operation{
							Hash:        oo.Hash,
							BlockHash:   b.Hash,
							NetworkHash: b.ChainID,
							Type: OperationType{
								Kind:   c.Kind,
								Source: this.getAccount(c.Source),
								Operations: []OperationTypeOperation{
									{
										// there is so much in here that does not make sense.
										// at all.
										Kind:          c.Kind,
										Src:           this.getAccount(c.Source),
										ManagerPubkey: this.getAccount(c.ManagerPublicKey),
										Balance:       "0",
										Spendable:     true,
										Delegatable:   true,
										Tz1:           this.getAccount(c.Source),
										Failed:        false,
										Fee:           c.Fee,
										Destination:   this.getAccount(c.Destination),
										Amount:        c.Amount,
										Counter:       counter,
										Internal:      false,
										OpLevel:       b.Header.Level,
										GasLimit:      c.GasLimit,
										BurnTez:       "",
										StorageLimit:  c.StorageLimit,
										Timestamp:     b.Header.Timestamp,
									},
								},
							},
						})
					}
				}
			}
		}
	}
	c.JSON(200, res)
}

func (this *Api) opAddr2(c *gin.Context) {
	kind := c.Query("type")      // required
	delegate := c.Param("param") // required

	fmt.Println(kind, delegate)
	qry := this.conseil.NewQuery()
	qry.AddFields("operation_group_hash", "block_hash")
	qry.AddPredicate("kind", "eq", []string{strings.ToLower(kind)}, false)
	qry.AddPredicate("source", "eq", []string{delegate}, false)
	qry.SetLimit(100)

	qres, err := this.conseil.Exec(qry, "operations")
	if err != nil || qry == nil {
		c.String(500, "internal error")
	}
	qres.Wait()
	model := []ConOp{}
	json.Unmarshal(qres.Bytes(), &model)

	blocks := map[string]bool{}

	for _, i := range model {
		blocks[i.BlockHash] = true
	}

	res := []Operation{}

	for i, _ := range blocks {
		b, _ := this.goTezos.Block.Get(i)
		for _, o := range b.Operations {
			for _, oo := range o {
				for _, c := range oo.Contents {
					counter, _ := strconv.Atoi(c.Counter)
					if c.Kind == strings.ToLower(kind) && c.Source == delegate {
						res = append(res, Operation{
							Hash:        oo.Hash,
							BlockHash:   b.Hash,
							NetworkHash: b.ChainID,
							Type: OperationType{
								Kind:   c.Kind,
								Source: this.getAccount(c.Source),
								Operations: []OperationTypeOperation{
									{
										// there is so much in here that does not make sense.
										// at all.
										Kind:          c.Kind,
										Src:           this.getAccount(c.Source),
										ManagerPubkey: this.getAccount(c.ManagerPublicKey),
										Balance:       "0",
										Spendable:     true,
										Delegatable:   true,
										Tz1:           this.getAccount(c.Source),
										Failed:        false,
										Fee:           c.Fee,
										Destination:   this.getAccount(c.Destination),
										Amount:        c.Amount,
										Counter:       counter,
										Internal:      false,
										OpLevel:       b.Header.Level,
										GasLimit:      c.GasLimit,
										BurnTez:       "",
										StorageLimit:  c.StorageLimit,
										Timestamp:     b.Header.Timestamp,
									},
								},
							},
						})
					}
				}
			}
		}
	}
	c.JSON(200, res)
}
