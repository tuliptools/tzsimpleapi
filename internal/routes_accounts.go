package internal

import (
	"encoding/json"
	"fmt"
	conv2 "github.com/cstockton/go-conv"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"math"
	"net/http"
	"strconv"
)

func (this *Api) rewardsSplit(c *gin.Context) {

	// WIP

	res := RewardsSplit{}
	del := c.Param("account_hash")
	cycle := this.validateInt(c, "cycle", 1)

	if cycle == 1 {
		c.String(200, "please set cycle")
		return
	}

	stakebal, _ := this.goTezos.Delegate.GetStakingBalanceAtCycle(del, cycle)

	res.DelegateStakingBalance = stakebal
	dels, _ := this.goTezos.Delegate.GetDelegationsAtCycle(del, cycle)
	snap, _ := this.goTezos.SnapShot.Get(cycle)
	snaphash := snap.AssociatedHash

	for _, d := range dels {
		url := this.config.TezosHost + "chains/main/blocks/" + snaphash + "/context/contracts/" + d + "/balance"
		res2, _ := http.Get(url)
		resbytes, _ := ioutil.ReadAll(res2.Body)
		res.DelegatorsBalance = append(res.DelegatorsBalance, DelBalance{
			Account: Hash{Tz: d},
			Balance: string(resbytes)[1 : len(resbytes)-2],
		})
	}

	res.DelegatorsNb = len(dels)

	c.JSON(200, res)

}

func (this *Api) accountFromAlias(c *gin.Context) {
	alias := c.Param("alias")
	if len(alias) <= 3 {
		c.String(500, "Not a valid account name")
		return
	}
	for _, ka := range this.knownAccounts {
		if this.cleanString(ka.Name) == this.cleanString(alias) {
			r := AccountFromAliasResponse{}
			r.Address = ka.Address
			c.JSON(200, r)
			return
		}
	}
	c.String(404, "Not found")
}

func (this *Api) accountStatus(c *gin.Context) {
	addr := c.Param("account_hash")
	res := AccountStatusResponse{}
	res.Hash.Tz = addr
	for _, ka := range this.knownAccounts {
		if this.cleanString(ka.Name) == this.cleanString(addr) {
			res.Hash.Alias = ka.Name
		}
	}

	c.JSON(200, res)
}

func (this *Api) numAccounts(c *gin.Context) {
	contract := this.validateBool(c, "contract", false)
	qry := this.conseil.NewQuery()
	qry.AddFields("account_id")
	qry.AddPredicate("account_id", "startsWith", []string{"KT1"}, !contract)
	qry.AddAggregation("account_id", "count")
	qry.SetLimit(100)
	res, err := this.conseil.Exec(qry, "accounts")
	if err != nil {
		c.String(500, "Internal Error")
		return
	}
	res.Wait()
	resmodel := ConAccountCounts{}
	json.Unmarshal(res.Bytes(), &resmodel)

	c.JSON(200, []int{resmodel[0].CountAccountID})

}

func (this *Api) accounts(c *gin.Context) {

	// TODO page

	p := this.validateInt(c, "p", 0)
	number := this.validateInt(c, "number", 100)
	contract := this.validateBool(c, "contract", false)

	if number >= 500 {
		number = 500
	}
	startswith := "tz"
	if contract {
		startswith = "KT"
	}
	qry := this.conseil.NewQuery()
	qry.AddFields("account_id", "manager", "spendable", "delegate_setable")
	qry.AddPredicate("account_id", "startsWith", []string{startswith}, false)
	qry.SetLimit(number).SetPage(p)

	res, err := this.conseil.Exec(qry, "accounts")
	if err != nil {
		c.String(500, "Internal Error")
		return
	}
	res.Wait()
	acc := ConAccounts{}
	json.Unmarshal(res.Bytes(), &acc)

	accounts := []AccountResponse{}
	for _, i := range acc {
		accounts = append(accounts, AccountResponse{
			Hash:         this.getAccount(i.AccountID),
			Manager:      this.getAccount(i.Manager),
			Spendable:    i.Spendable,
			Delegateable: i.DelegateSetable,
		})
	}

	c.JSON(200, accounts)

}

func (this *Api) balance(c *gin.Context) {
	addr := c.Param("account_hash")
	balance, _ := this.goTezos.Account.GetBalance(addr)
	balanceStr, _ := conv2.String(uint64(balance * 1000000))
	c.JSON(200, []string{balanceStr})
}

func (this *Api) balanceExtended(c *gin.Context) {
	nbr := func(s string) uint64 {
		u, _ := conv2.Uint64(s)
		return u
	}
	addr := c.Param("account_hash")
	balance, _ := this.goTezos.Delegate.GetDelegate(addr)
	res := ExtendedBalance{}
	res.Spendable = nbr(balance.Balance) - nbr(balance.FrozenBalance)
	res.Frozen = nbr(balance.FrozenBalance)
	for _, a := range balance.FrozenBalanceByCycle {
		res.Rewards += nbr(a.Rewards)
		res.Fees += nbr(a.Fees)
		res.Deposits += nbr(a.Deposit)
	}
	c.JSON(200, res)
}

func (this *Api) staking_balance(c *gin.Context) {
	addr := c.Param("account_hash")

	nbr := func(s string) uint64 {
		u, _ := conv2.Uint64(s)
		return u
	}

	balance, _ := this.goTezos.Delegate.GetDelegate(addr)
	res := ExtendedBalance{}
	res.Spendable = nbr(balance.Balance) - nbr(balance.FrozenBalance)
	res.Frozen = nbr(balance.FrozenBalance)
	for _, a := range balance.FrozenBalanceByCycle {
		res.Rewards += nbr(a.Rewards)
		res.Fees += nbr(a.Fees)
		res.Deposits += nbr(a.Deposit)
	}

	bstr, _ := conv2.String(balance.StakingBalance)
	c.JSON(200, []string{bstr})
}

func (this *Api) balanceNumber(c *gin.Context) {

	c.String(200, "low priority")
}

func (this *Api) ballots(c *gin.Context) {

}

func (this *Api) numBalanceUpdates(c *gin.Context) {
	// TODO
	res := 0
	cycle := this.validateInt(c, "cycle", 158)

	qry := this.conseil.NewQuery()
	qry.AddFields("id")
	qry.AddPredicate("level", "lt", []string{strconv.Itoa(cycle * 4096)}, false)
	qry.AddPredicate("level", "gt", []string{strconv.Itoa((cycle - 1) * 4096)}, false)
	qry.AddPredicate("source", "eq", []string{c.Param("account_hash")}, false)
	qry.AddAggregation("id", "count")
	qry.SetLimit(100)

	qres, err := this.conseil.Exec(qry, "balance_updates")
	if err != nil || qry == nil {
		c.String(500, "internal error")
		return
	}
	qres.Wait()

	fmt.Println(qres.String())

	c.JSON(200, []int{res})
}

func (this *Api) numberOps(c *gin.Context) {
	kind := c.Param("type")

	qry := this.conseil.NewQuery()
	qry.AddFields("operation_group_hash")
	if kind != "" {
		qry.AddPredicate("kind", "eq", []string{kind}, false)
	}
	qry.AddAggregation("operation_group_hash", "count")
	qry.SetLimit(100)
	res, err := this.conseil.Exec(qry, "operations")

	if err != nil {
		c.String(500, "Internal Error")
		return
	}
	res.Wait()

	resmodel := ConOpGroup{}
	json.Unmarshal(res.Bytes(), &resmodel)
	c.JSON(200, []uint64{resmodel[0].Count})
}

func (this *Api) bhToVotingNumbers(bh int) (int, string) {
	blocksPerCycle := 4096
	cyclesVotingPeriod := 8 * blocksPerCycle
	period := bh / cyclesVotingPeriod
	names := []string{
		"proposal",
		"exploration_vote",
		"testing_period",
		"promotion_vote",
	}
	return period, names[period%4]
}

func (this *Api) votesAccount(c *gin.Context) {

	res := []AccountVote{}

	qry := this.conseil.NewQuery()
	qry.AddFields("source", "ballot", "proposal", "operation_group_hash", "cycle", "level")
	qry.AddPredicate("kind", "eq", []string{"ballot"}, false)
	qry.AddPredicate("source", "eq", []string{c.Param("account_hash")}, false)
	qry.SetLimit(100)

	qres, err := this.conseil.Exec(qry, "operations")
	if err != nil || qry == nil {
		c.String(500, "internal error")
		return
	}
	qres.Wait()
	model := []ConVote{}
	json.Unmarshal(qres.Bytes(), &model)

	period, periodname := this.bhToVotingNumbers((model[0].Cycle * 4096) - 1)

	for _, i := range model {
		del, _ := this.goTezos.Delegate.GetStakingBalance(c.Param("account_hash"), i.Cycle)
		n := AccountVote{
			Source:       this.getAccount(i.Source),
			ProposalHash: i.Proposal,
			Operation:    i.OperationGroupHash,
			Ballot:       i.Ballot,
			Count:        1,
			PeriodKind:   periodname,
			VotingPeriod: period,
		}

		n.Votes = int(math.Floor(del / 8000))
		res = append(res, n)

	}

	c.JSON(200, res)
}
func (this *Api) rollNumber(c *gin.Context) {
	addr := c.Param("account_hash")
	balance, _ := this.goTezos.Delegate.GetDelegate(addr)
	sb, _ := strconv.Atoi(balance.StakingBalance)
	c.JSON(200, []int{(sb / 1000000) / 8000})
}

func (this *Api) balanceUpdates(c *gin.Context) {

	addr := c.Param("account_hash")
	cycle := this.validateInt(c, "cycle", 1)
	res := []BalanceUpdate{}

	qry := this.conseil.NewQuery()
	qry.AddFields("source", "source_id", "source_hash", "kind", "contract", "change", "level", "delegate")
	qry.AddPredicate("contract", "eq", []string{addr}, false)
	qry.AddPredicate("level", "gt", []string{strconv.Itoa(cycle * 4096)}, false)
	qry.AddPredicate("level", "lt", []string{strconv.Itoa(cycle + 1*4096)}, false)
	qry.SetLimit(10)

	qres, err := this.conseil.Exec(qry, "balance_updates")
	if err != nil || qry == nil {
		c.String(500, "internal error")
		return
	}
	qres.Wait()
	fmt.Println(qres.String())

	c.JSON(200, res)
}

func (this *Api) getAccount(addr string) Hash {
	res := Hash{}
	res.Tz = addr

	for _, ka := range this.knownAccounts {
		if this.cleanString(ka.Name) == this.cleanString(addr) {
			res.Alias = ka.Name
		}
	}

	return res
}

func (this *Api) validateInt(c *gin.Context, key string, onerr int) int {
	param := c.DefaultQuery(key, "notexists")
	if param == "notexists" {
		return onerr
	} else {
		res, err := strconv.Atoi(param)
		if err != nil {
			return onerr
		}
		return res
	}
}

func (this *Api) validateBool(c *gin.Context, key string, onerr bool) bool {
	param := c.DefaultQuery(key, "notexists")
	if param == "notexists" {
		return onerr
	} else {
		res, err := conv2.Bool(param)
		if err != nil {
			return onerr
		}
		return res
	}
}
