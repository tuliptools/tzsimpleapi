package internal

import "time"

type ConAccounts []struct {
	AccountID       string `json:"account_id"`
	Manager         string `json:"manager"`
	Spendable       bool   `json:"spendable"`
	DelegateSetable bool   `json:"delegate_setable"`
}

type RpcBakingRights struct {
	Level         int       `json:"level"`
	Delegate      string    `json:"delegate"`
	Priority      int       `json:"priority"`
	EstimatedTime time.Time `json:"estimated_time"`
}

type RPCEndrosementRights struct {
	Level         int       `json:"level"`
	Delegate      string    `json:"delegate"`
	Slots         []int     `json:"slots"`
	EstimatedTime time.Time `json:"estimated_time,omitempty"`
}

type ConBlocks struct {
	Priority    int    `json:"priority"`
	Timestamp   int64  `json:"timestamp"`
	Baker       string `json:"baker"`
	MetaCycle   int    `json:"meta_cycle"`
	Hash        string `json:"hash"`
	Level       int    `json:"level"`
	Predecessor string `json:"predecessor"`
}

type ConEndorsement struct {
	NumberOfSlots int         `json:"number_of_slots"`
	Pkh           interface{} `json:"pkh"`
	Slots         string      `json:"slots"`
	Kind          string      `json:"kind"`
	Level         int         `json:"level"`
	BlockHash     string      `json:"bock_hash"`
	Timestamp     int64       `json:"timestamp"`
	Source        string      `json:"source"`
}

type ConAccountCounts []struct {
	CountAccountID int `json:"count_account_id,string"`
}

type ConOpGroup []struct {
	Count uint64 `json:"count_operation_group_hash,string"`
}

type ConVote struct {
	Source             string `json:"source"`
	Ballot             string `json:"ballot"`
	Proposal           string `json:"proposal"`
	OperationGroupHash string `json:"operation_group_hash"`
	Cycle              int    `json:"cycle"`
	Level              int    `json:"level"`
}

type ConOp struct {
	OperationGroupHash string `json:"operation_group_hash"`
	BlockHash          string `json:"block_hash"`
}
