package internal

import (
	"github.com/gin-gonic/gin"
)

func (this *Api) routes(router *gin.Engine) {
	// DONE
	empty := func(context *gin.Context) {
		context.String(200, "[]")
	}
	router.GET("/v3/account_from_alias/:alias", this.accountFromAlias)
	router.GET("/v3/account_status/:account_hash", this.accountStatus)
	router.GET("/v3/accounts", this.accounts)
	router.GET("/v3/baker_rights/:account_hash", this.bakerRights)
	router.GET("/v3/bakings/:account_hash", this.bakings)
	router.GET("/v3/bakings_endorsement/:account_hash", this.baking_endorsements)
	router.GET("/v3/balance/:account_hash", this.balance)
	router.GET("/v3/balance_from_balance_updates/:account_hash", this.balanceExtended)
	router.GET("/v3/balance_history/:account_hash", empty)
	router.GET("/v3/balance_updates/:account_hash", this.balanceUpdates)
	router.GET("/v3/head", this.head)
	router.GET("/v3/genesis", this.genesis)
	router.GET("/v3/constants", this.constants)
	router.GET("/v3/heads", this.head)
	router.GET("/v3/block/:block_hash", this.block)
	router.GET("/v3/network/:block_hash", this.blockChainId)
	router.GET("/v3/operations/:param", this.operationEntry)
	router.GET("/v3/level/:block_hash", this.level)
	router.GET("/v3/roll_number/:account_hash", this.rollNumber)
	router.GET("/v3/endorser_rights/:account_hash", this.endorserRights)
	router.GET("/v3/block_level/:level", this.blockLevel)
	router.GET("/v3/block_next/:block_hash", this.blockNext)
	router.GET("/v3/block_prev/:block_hash", this.blocksPrev)
	router.GET("/v3/block_hash_level/:level", this.blockHashLevel)
	router.GET("/v3/volume/:block_hash", this.volume)
	router.GET("/v3/timestamp/:block_hash", this.timestamp)
	router.GET("/v3/priority/:block_hash", this.priority)
	router.GET("/v3/number_operations/:hash", this.numOps)
	router.GET("/v3/number_accounts", this.numAccounts)
	router.GET("/v3/votes_account/:account_hash", this.votesAccount)
	router.GET("/v3/ballots/:period", this.ballots)
	router.GET("/v3/cycle_baker_rights/:account_hash", this.dummyHandler)
	router.GET("/v3/number_baker_rights/:account_hash", this.numBakingRights)
	router.GET("/v3/number_baking_rights", this.dummyHandler)
	router.GET("/v3/number_bakings/:account_hash", this.numBakings)
	router.GET("/v3/number_bakings_endorsement/:account_hash", this.numEndorsements)
	router.GET("/v3/number_operations", this.numberOps)
	router.GET("/v3/balance_updates_number/:account_hash", this.numBalanceUpdates)
	router.GET("/v3/operations", this.operationsAddr)
	router.GET("/v3/blocks", this.blocks)
	router.GET("/v3/staking_balance/:account_hash", this.staking_balance)
	router.GET("/v1/block/:level", this.blockLevel)
	router.GET("/v3/rewards_split/:account_hash", this.rewardsSplit)

	router.GET("/v3/delegator_rewards_with_details/:contract_hash", this.dummyHandler)

	// tw

	// priority

	//
	// router.GET("/v3/bonds_rewards/:account_hash", this.dummyHandler)
	// router.GET("/v3/cycle_bakings/:account_hash", this.dummyHandler)
	// router.GET("/v3/cycle_endorsements/:account_hash", this.dummyHandler)
	// router.GET("/v3/cycle_endorser_rights/:account_hash", this.dummyHandler)
	// router.GET("/v3/cycle_frozen/:account_hash", this.dummyHandler)
	// router.GET("/v3/snapshot_blocks", this.dummyHandler)
	// router.GET("/v3/snapshot_levels", this.dummyHandler)
	// router.GET("/v3/extra_bonds_rewards/:account_hash", this.dummyHandler)
	// router.GET("/v3/endorsements/:level", this.dummyHandler)
	// router.GET("/v3/operation/:op_hash", this.dummyHandler)
	// router.GET("/v3/delegator_rewards/:contract_hash", this.dummyHandler)
	// router.GET("/v3/delegator_rewards_with_details/:contract_hash", this.dummyHandler)
	// router.GET("/v3/next_baking_and_endorsement/:account_hash", this.dummyHandler)

	router.GET("/v3/number_endorser_rights/:account_hash", this.dummyHandler)
	router.GET("/v3/ballot_votes/:proposal_hash", this.dummyHandler)
	router.GET("/v3/last_baking_and_endorsement/:account_hash", this.dummyHandler)
	router.GET("/v3/nb_delegators/:account_hash", this.dummyHandler)
	router.GET("/v3/nb_proposal_votes/:proposal_hash", this.dummyHandler)
	router.GET("/v3/period_summary", this.dummyHandler)
	router.GET("/v3/proposal_votes/:proposal_hash", this.dummyHandler)
	router.GET("/v3/required_balance/:account_hash", this.dummyHandler)
	router.GET("/v3/rewards_split_cycles/:account_hash", this.dummyHandler)
	router.GET("/v3/rolls_history/:account_hash", this.dummyHandler)
	router.GET("/v3/testing_protocol/:period", this.dummyHandler)
	router.GET("/v3/total_bakings/:account_hash", this.dummyHandler)
	router.GET("/v3/total_endorsements/:account_hash", this.dummyHandler)
	router.GET("/v3/total_proposal_votes/:period", this.dummyHandler)
	router.GET("/v3/total_voters/:period", this.dummyHandler)
	router.GET("/v3/voting_period_info", this.dummyHandler)
	router.GET("/v3/active_balance_updates/:account_hash", empty)
	router.GET("/v3/baking_rights", this.dummyHandler)

	router.GET("/v3/proposals", empty)
	router.GET("/v3/protocols", empty)
	router.GET("/v3/nb_ballot_votes/:proposal_hash", empty)
	router.GET("/v3/nb_cycle_delegator_rewards/:contract_hash", empty)
	router.GET("/v3/nb_cycle_rewards/:account_hash", empty)
	router.GET("/v3/nb_proposals", empty)
	router.GET("/v3/nb_protocol", empty)
	router.GET("/v3/nb_snapshot_blocks", this.dummyHandler)
	router.GET("/v3/nb_uncles/:level", empty)
	router.GET("/v3/nonces", empty)

	router.GET("/v3/balance_number", func(context *gin.Context) {
		context.JSON(200, "[0]")
	})

	router.GET("/v3/nb_heads", func(context *gin.Context) {
		context.String(200, "[0]")
	})

	router.GET("/v3/balance_ranking", this.dummyHandler)

	router.GET("/apidesc", func(context *gin.Context) {
		context.String(200, `
		TzSimpleApi

		Implemented methods:

		/v3/account_from_alias/:alias
		/v3/account_status/:account_hash
		/v3/accounts
		/v3/baker_rights/:account_hash
		/v3/bakings/:account_hash
		/v3/bakings_endorsement/:account_hash
		/v3/balance/:account_hash
		/v3/balance_from_balance_updates/:account_hash
		/v3/balance_history/:account_hash
		/v3/balance_updates/:account_hash
		/v3/head
		/v3/genesis
		/v3/constants
		/v3/heads
		/v3/block/:block_hash
		/v3/network/:block_hash
		/v3/operations/:param
		/v3/level/:block_hash
		/v3/roll_number/:account_hash
		/v3/endorser_rights/:account_hash
		/v3/block_level/:level
		/v3/block_next/:block_hash
		/v3/block_prev/:block_hash
		/v3/block_hash_level/:level
		/v3/volume/:block_hash
		/v3/timestamp/:block_hash
		/v3/priority/:block_hash
		/v3/number_operations/:hash
		/v3/number_accounts
		/v3/votes_account/:account_hash
		/v3/ballots/:period
		/v3/cycle_baker_rights/:account_hash
		/v3/number_baker_rights/:account_hash
		/v3/number_baking_rights
		/v3/number_bakings/:account_hash
		/v3/number_bakings_endorsement/:account_hash
		/v3/number_operations
		/v3/balance_updates_number/:account_hash
		/v3/operations
		/v3/blocks
		/v3/staking_balance/:account_hash
		/v1/block/:level
		/v3/rewards_split/:account_hash

		`)
	})

}
