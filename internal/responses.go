package internal

import (
	gotezos "github.com/DefinitelyNotAGoat/go-tezos"
	"time"
)

type AccountFromAliasResponse struct {
	Address string
}

type AccountStatusResponse struct {
	Hash        Hash   `json:"hash"`
	Revelation  string `json:"revelation,omitempty"`
	Origination string `json:"origination,omitempty"`
}

type Hash struct {
	Tz    string `json:"tz"`
	Alias string `json:"alias"`
}

type AccountResponse struct {
	Hash         Hash `json:"hash"`
	Manager      Hash `json:"manager"`
	Spendable    bool `json:"spendable"`
	Delegateable bool `json:"delegatable"`
}

type BakerRights struct {
	Level    int `json:"level"`
	Cycle    int `json:"cycle"`
	Priority int `json:"priority"`
	Depth    int `json:"depth"`
}

type Bakings struct {
	BlockHash      string    `json:"block_hash"`
	BakerHash      Hash      `json:"baker_hash"`
	Level          int       `json:"level"`
	Cycle          int       `json:"cycle"`
	Priority       int       `json:"priority"`
	MissedPriority int       `json:"missed_priority,omitempty"`
	DistanceLevel  int       `json:"distance_level"`
	Fees           uint64    `json:"fees"`
	BakeTime       int       `json:"bake_time"`
	Baked          bool      `json:"baked"`
	Timestamp      time.Time `json:"timestamp"`
}

type BakingsEndorsement struct {
	Block         string    `json:"block"`
	Source        Hash      `json:"source"`
	Level         int       `json:"level"`
	Cycle         int       `json:"cycle"`
	Priority      int       `json:"priority"`
	DistanceLevel int       `json:"distance_level"`
	Slots         []int     `json:"slots"`
	LrNslot       int       `json:"lr_nslot"`
	Timestamp     time.Time `json:"timestamp"`
}

type ExtendedBalance struct {
	Spendable uint64 `json:"spendable,string"`
	Frozen    uint64 `json:"frozen,string"`
	Rewards   uint64 `json:"rewards,string"`
	Fees      uint64 `json:"fees,string"`
	Deposits  uint64 `json:"deposits,string"`
}

type BalanceUpdate struct {
	Account    string    `json:"account"`
	Block      string    `json:"block"`
	Diff       string    `json:"diff"`
	Date       time.Time `json:"date"`
	UpdateType string    `json:"update_type"`
	OpType     string    `json:"op_type"`
	Internal   bool      `json:"internal"`
	Frozen     bool      `json:"frozen"`
	Level      int       `json:"level"`
	Burn       bool      `json:"burn"`
}

type Head struct {
	Hash            string                       `json:"hash"`
	PredecessorHash string                       `json:"predecessor_hash"`
	Fitness         string                       `json:"fitness"`
	Timestamp       time.Time                    `json:"timestamp"`
	ValidationPass  int                          `json:"validation_pass"`
	Operations      [][]gotezos.StructOperations `json:"operationsBH"`
	Protocol        struct {
		Name string `json:"name"`
		Hash string `json:"hash"`
	} `json:"protocol"`
	TestProtocol struct {
		Name string `json:"name"`
		Hash string `json:"hash"`
	} `json:"test_protocol"`
	Network               string `json:"network"`
	TestNetwork           string `json:"test_network"`
	TestNetworkExpiration string `json:"test_network_expiration"`
	Baker                 Hash   `json:"baker"`
	NbOperations          int    `json:"nb_operations"`
	Priority              int    `json:"priority"`
	Level                 int    `json:"level"`
	CommitedNonceHash     string `json:"commited_nonce_hash"`
	PowNonce              string `json:"pow_nonce"`
	Proto                 int    `json:"proto"`
	Data                  string `json:"data"`
	Signature             string `json:"signature"`
	Volume                uint64 `json:"volume,string"`
	Fees                  uint64 `json:"fees,string"`
	DistanceLevel         int    `json:"distance_level"`
	Cycle                 int    `json:"cycle"`
}

type SimpleOperation struct {
	Hash        string `json:"hash"`
	BlockHash   string `json:"block_hash"`
	NetworkHash string `json:"network_hash"`
	Type        OpType `json:"type"`
}

type OpType struct {
	Kind      string `json:"kind"`
	Block     string `json:"block"`
	Level     int    `json:"level"`
	Endorser  Hash   `json:"endorser"`
	Slots     []int  `json:"slots"`
	OpLevel   int    `json:"op_level"`
	Priority  int    `json:"priority"`
	Timestamp string `json:"timestamp"`
}

type Level struct {
	Level                int  `json:"level"`
	LevelPosition        int  `json:"level_position"`
	Cycle                int  `json:"cycle"`
	CyclePosition        int  `json:"cycle_position"`
	VotingPeriod         int  `json:"voting_period"`
	VotingPeriodPosition int  `json:"voting_period_position"`
	ExpectedCommitment   bool `json:"expected_commitment"`
}

type EndorserRights struct {
	Level int `json:"level"`
	Cycle int `json:"cycle"`
	Nslot int `json:"nslot"`
	Depth int `json:"depth"`
}

type AccountVote struct {
	VotingPeriod int    `json:"voting_period"`
	PeriodKind   string `json:"period_kind"`
	ProposalHash string `json:"proposal_hash"`
	Count        int    `json:"count"`
	Votes        int    `json:"votes"`
	Source       Hash   `json:"source"`
	Operation    string `json:"operation"`
	Ballot       string `json:"ballot"`
}

type Operation struct {
	Hash        string        `json:"hash"`
	BlockHash   string        `json:"block_hash"`
	NetworkHash string        `json:"network_hash"`
	Type        OperationType `json:"type"`
}

type OperationType struct {
	Kind       string                   `json:"kind"`
	Source     Hash                     `json:"source"`
	Operations []OperationTypeOperation `json:"operations"`
}

type OperationTypeOperation struct {
	Kind          string        `json:"kind"`
	Src           Hash          `json:"src"`
	ManagerPubkey Hash          `json:"managerPubkey"`
	Balance       string        `json:"balance"`
	Spendable     bool          `json:"spendable"`
	Delegatable   bool          `json:"delegatable"`
	Tz1           Hash          `   json:"tz1"`
	Failed        bool          `json:"failed"`
	Internal      bool          `json:"internal"`
	BurnTez       string        `json:"burn_tez"`
	Counter       int           `json:"counter"`
	Fee           string        `json:"fee"`
	GasLimit      string        `json:"gas_limit"`
	StorageLimit  string        `json:"storage_limit"`
	Destination   Hash          `json:"destination"`
	Amount        string        `json:"amount"`
	OpLevel       int           `json:"op_level"`
	Timestamp     time.Time     `json:"timestamp"`
	Errors        []interface{} `json:"errors"`
}

type RewardsSplit struct {
	DelegateStakingBalance                  string       `json:"delegate_staking_balance"`
	DelegatorsNb                            int          `json:"delegators_nb"`
	DelegatorsBalance                       []DelBalance `json:"delegators_balance"`
	BlocksRewards                           string       `json:"blocks_rewards"`
	EndorsementsRewards                     string       `json:"endorsements_rewards"`
	Fees                                    string       `json:"fees"`
	FutureBlocksRewards                     string       `json:"future_blocks_rewards"`
	FutureEndorsementsRewards               string       `json:"future_endorsements_rewards"`
	GainFromDenounciationBaking             string       `json:"gain_from_denounciation_baking"`
	LostDepositFromDenounciationBaking      string       `json:"lost_deposit_from_denounciation_baking"`
	LostRewardsDenounciationBaking          string       `json:"lost_rewards_denounciation_baking"`
	LostFeesDenounciationBaking             string       `json:"lost_fees_denounciation_baking"`
	GainFromDenounciationEndorsement        string       `json:"gain_from_denounciation_endorsement"`
	LostDepositFromDenounciationEndorsement string       `json:"lost_deposit_from_denounciation_endorsement"`
	LostRewardsDenounciationEndorsement     string       `json:"lost_rewards_denounciation_endorsement"`
	LostFeesDenounciationEndorsement        string       `json:"lost_fees_denounciation_endorsement"`
	RevelationRewards                       string       `json:"revelation_rewards"`
	LostRevelationRewards                   string       `json:"lost_revelation_rewards"`
	LostRevelationFees                      string       `json:"lost_revelation_fees"`
}

type DelBalance struct {
	Account Hash   `json:"account"`
	Balance string `json:"balance"`
}
