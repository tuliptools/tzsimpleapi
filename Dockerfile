#first stage - builder
FROM golang:1.13.0-stretch as builder
COPY . /project
WORKDIR /project
ENV GO111MODULE=on
RUN CGO_ENABLED=0 GOOS=linux go build -o tzsapi
#second stage

FROM alpine:latest
WORKDIR /root/
COPY --from=builder /project .
CMD ["./tzsapi"]