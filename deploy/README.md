# docker-compose deploy

The docker-compose file describes a simple setup of tzsimpleapi

1.) Create a identity file dor the tezos archive node

```
 docker-compose up idgen
```
 
2.) start the tezos node
 
 ```
 docker-compose up -d tezos
 ```
 
3.) start conseil ( db + indexer + api)
 
```
docker-compose up -d postgres
docker-compose up -d conseil-indexer
docker-compose up -d conseil-api
```
 
 
 
4.) start tzsimpleapi
 
```
docker-compose up -d tzsimple
```


 
 