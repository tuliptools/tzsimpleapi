package main

import (
	"fmt"
	goTezos "github.com/DefinitelyNotAGoat/go-tezos"
	"gitlab.com/tuliptools/tzsimplepi/conseil"
	"gitlab.com/tuliptools/tzsimplepi/internal"
	"go.uber.org/dig"
)

func main() {
	fmt.Println("TEZ SIMPLE API 0.1")

	c := dig.New()

	mustnil := func(err error) {
		if err != nil {
			panic(fmt.Errorf(err.Error()))
		}
	}

	mustnil(c.Provide(internal.NewTzSimpleConfig))
	mustnil(c.Provide(internal.NewAPI))
	mustnil(c.Provide(func(conf *internal.TzSimpleConfig) *conseil.Conseil {
		// create conseil with default http client settings ( nil )
		return conseil.NewConseil(conf.ConseilHost, conf.ConseilNetwork, conf.ConseilToken, conf.ConseilPlatform, nil)
	}))

	mustnil(c.Provide(func(conf *internal.TzSimpleConfig) *goTezos.GoTezos {
		// create conseil with default http client settings ( nil )
		fmt.Println(conf.TezosHost)
		res, err := goTezos.NewGoTezos(conf.TezosHost, "dummy")
		if err != nil {
			fmt.Println("error with gotez")
			panic(err)
		}
		return res
	}))

	err := c.Invoke(func(api *internal.Api) {
		api.Run()

	})

	fmt.Println(err)
}
